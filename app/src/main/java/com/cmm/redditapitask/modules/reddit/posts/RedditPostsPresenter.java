package com.cmm.redditapitask.modules.reddit.posts;

import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.data.source.local.LocalDataSource;
import com.cmm.redditapitask.data.source.remote.RemoteDataSource;
import com.cmm.redditapitask.data.source.remote.callbacks.GetRedditResponseCallback;

import java.util.List;

/**
 * Created by ankit on 09/12/17.
 */

public class RedditPostsPresenter implements RedditPostsContract.Presenter {

    private RedditPostsContract.View mView;
    private RemoteDataSource mRemoteDataSource;
    private LocalDataSource mLocalDataSource;
    private static final String TAG = RedditPostsPresenter.class.getSimpleName();

    public RedditPostsPresenter(RedditPostsContract.View view, LocalDataSource localDataSource, RemoteDataSource remoteDataSource){

        this.mView = view;
        this.mLocalDataSource = localDataSource;
        this.mRemoteDataSource = remoteDataSource;

        this.mView.setPresenter(this);
    }

    @Override
    public void init() {

    }

    @Override
    public void bindView(RedditPostsContract.View view) {
        this.mView = view;
    }

    @Override
    public void unbindView() {
        this.mView = null;
    }

    @Override
    public void getNewRedditPosts(int count, int limit) {

        this.mView.showLoader();
        mRemoteDataSource.getNewRedditPosts(count, limit, new GetRedditResponseCallback<List<RedditPost>>() {


            @Override
            public void onError(Throwable throwable) {

                if(throwable != null) mView.onError(throwable);
            }

            @Override
            public void onSuccess(List<RedditPost> posts, RedditPage page) {

                mView.hideLoader();
                mView.onNewRedditPostsFetched(posts, page);
            }
        });
    }
}
