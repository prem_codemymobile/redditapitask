package com.cmm.redditapitask.modules.reddit.posts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmm.redditapitask.R;
import com.cmm.redditapitask.adapters.RedditPostsRecyclerViewAdapter;
import com.cmm.redditapitask.core.BaseFragment;
import com.cmm.redditapitask.core.PresenterManager;
import com.cmm.redditapitask.data.listener.OnSelectRedditPostListener;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.data.source.local.LocalDataSource;
import com.cmm.redditapitask.data.source.remote.RemoteDataSource;
import com.cmm.redditapitask.data.statics.Constant;
import com.cmm.redditapitask.data.statics.Extra;
import com.cmm.redditapitask.modules.reddit.comments.RedditCommentsFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ankit on 09/12/17.
 */

public class RedditPostsFragment extends BaseFragment implements RedditPostsContract.View, OnSelectRedditPostListener {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.loader)
    android.widget.ProgressBar mProgressSpinner;

    private RedditPostsContract.Presenter mPresenter;
    private static final String TAG = RedditPostsFragment.class.getSimpleName();
    private RedditPostsRecyclerViewAdapter mRecyclerViewAdapter;

    private int count = 0;
    private int limit = Constant.HOT_REDDIT_POSTS_FETCH_LIMIT;
    private boolean isPostsLoading;
    private boolean isLastPage;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        ButterKnife.bind(this, view);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        String presenterId = String.valueOf(hashCode());
        this.mPresenter = (RedditPostsContract.Presenter) PresenterManager.get(presenterId);
        if(this.mPresenter!=null){
            this.mPresenter.bindView(this);
        }else {
            this.mPresenter = new RedditPostsPresenter(this,
                    LocalDataSource.getInstance(getActivity()),
                    RemoteDataSource.getInstance(getActivity()));
            PresenterManager.set(presenterId, this.mPresenter);
        }

        initRecyclerView();
        mPresenter.getNewRedditPosts(count, limit);
        return view;
    }

    @Override
    public void setPresenter(RedditPostsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showLoader() {

        isPostsLoading = true;
        mProgressSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {

        isPostsLoading = false;
        mProgressSpinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onNewRedditPostsFetched(List<RedditPost> posts, RedditPage page) {
        Log.d(TAG, "onNewRedditPostsFetched: "+posts.size()+" reddit found");

        mRecyclerViewAdapter.setReddits(posts, count, true);
        if (posts.size() >= Constant.HOT_REDDIT_POSTS_FETCH_LIMIT) {
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void onError(Throwable t) {
        Log.d(TAG, "onError: "+String.valueOf(t.getMessage()));
    }

    private void initRecyclerView() {

        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        Log.d(TAG, "initRecyclerView: "+mRecyclerView);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                Log.d(TAG,"visible="+visibleItemCount+", total="+totalItemCount+", first="+firstVisibleItemPosition);

                if (!isPostsLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Constant.HOT_REDDIT_POSTS_FETCH_LIMIT) {

                        count += Constant.HOT_REDDIT_POSTS_FETCH_LIMIT;
                        mPresenter.getNewRedditPosts(count, limit);
                        Log.d(TAG, "Fetching reddit posts, page offset="+count);
                    }
                }
            }
        });

        mRecyclerViewAdapter = new RedditPostsRecyclerViewAdapter(getActivity());
        mRecyclerViewAdapter.setmListener(this);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    @Override
    public void onRedditPostSelected(RedditPost post) {

        Log.d(TAG, "onRedditPostSelected: "+post.getId());

        Bundle bundle = new Bundle();
        bundle.putCharSequence(Extra.REDDIT_POST_ID, post.getId());
        RedditCommentsFragment fragment = new RedditCommentsFragment();
        fragment.setArguments(bundle);

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        ft.add(R.id.container, fragment).commit();
    }
}
