package com.cmm.redditapitask.modules.reddit.posts;

import com.cmm.redditapitask.core.BasePresenter;
import com.cmm.redditapitask.core.BaseView;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;

import java.util.List;

/**
 * Created by ankit on 09/12/17.
 */

public class RedditPostsContract {

    interface View extends BaseView<RedditPostsContract.Presenter> {

        void showLoader();
        void hideLoader();
        void onNewRedditPostsFetched(List<RedditPost> list, RedditPage page);
        void onError(Throwable t);

    }

    interface Presenter extends BasePresenter<RedditPostsContract.View> {

        void getNewRedditPosts(int count, int limit);
    }

}
