package com.cmm.redditapitask.modules.reddit.comments;

import com.cmm.redditapitask.core.BasePresenter;
import com.cmm.redditapitask.core.BaseView;
import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.modules.reddit.posts.RedditPostsContract;

import java.util.List;

/**
 * Created by ankit on 10/12/17.
 */

public class RedditCommentsContract {

    interface View extends BaseView<RedditCommentsContract.Presenter> {

        void showLoader();
        void hideLoader();
        void onNewRedditCommentsFetched(List<RedditComment> list, RedditPage page);
        void onError(Throwable t);

    }

    interface Presenter extends BasePresenter<RedditCommentsContract.View> {

        void getNewRedditComments(String redditId, RedditPage page);
    }
}
