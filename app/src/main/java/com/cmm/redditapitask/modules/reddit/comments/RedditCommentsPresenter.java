package com.cmm.redditapitask.modules.reddit.comments;

import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.data.source.local.LocalDataSource;
import com.cmm.redditapitask.data.source.remote.RemoteDataSource;
import com.cmm.redditapitask.data.source.remote.callbacks.GetRedditResponseCallback;
import com.cmm.redditapitask.modules.reddit.posts.RedditPostsContract;
import com.cmm.redditapitask.modules.reddit.posts.RedditPostsPresenter;

import java.util.List;

/**
 * Created by ankit on 10/12/17.
 */

public class RedditCommentsPresenter implements RedditCommentsContract.Presenter {

    private RedditCommentsContract.View mView;
    private RemoteDataSource mRemoteDataSource;
    private LocalDataSource mLocalDataSource;
    private static final String TAG = RedditCommentsPresenter.class.getSimpleName();

    public RedditCommentsPresenter(RedditCommentsContract.View view, LocalDataSource localDataSource, RemoteDataSource remoteDataSource){

        this.mView = view;
        this.mLocalDataSource = localDataSource;
        this.mRemoteDataSource = remoteDataSource;

        this.mView.setPresenter(this);
    }

    @Override
    public void init() {

    }

    @Override
    public void bindView(RedditCommentsContract.View view) {
        this.mView = view;
    }

    @Override
    public void unbindView() {
        this.mView = null;
    }

    @Override
    public void getNewRedditComments(String redditId, RedditPage page) {

        this.mView.showLoader();
        mRemoteDataSource.getNewRedditComments(redditId, page, new GetRedditResponseCallback<List<RedditComment>>() {


            @Override
            public void onError(Throwable throwable) {

                if(throwable != null) mView.onError(throwable);
                mView.hideLoader();
            }

            @Override
            public void onSuccess(List<RedditComment> comments, RedditPage page) {

                mView.hideLoader();
                mView.onNewRedditCommentsFetched(comments, page);
            }
        });
    }
}
