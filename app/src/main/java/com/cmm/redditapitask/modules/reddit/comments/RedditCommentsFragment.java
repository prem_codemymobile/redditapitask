package com.cmm.redditapitask.modules.reddit.comments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmm.redditapitask.R;
import com.cmm.redditapitask.adapters.RedditCommentsRecyclerViewAdapter;
import com.cmm.redditapitask.core.BaseFragment;
import com.cmm.redditapitask.core.PresenterManager;
import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.source.local.LocalDataSource;
import com.cmm.redditapitask.data.source.remote.RemoteDataSource;
import com.cmm.redditapitask.data.statics.Constant;
import com.cmm.redditapitask.data.statics.Extra;
import com.cmm.redditapitask.modules.reddit.posts.RedditPostsFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ankit on 10/12/17.
 */

public class RedditCommentsFragment extends BaseFragment implements RedditCommentsContract.View {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.loader)
    android.widget.ProgressBar mProgressSpinner;

    @BindView(R.id.no_comment_text)
    TextView mNoCommentsText;


    private RedditCommentsContract.Presenter mPresenter;
    private RedditCommentsRecyclerViewAdapter mRecyclerViewAdapter;
    private static final String TAG = RedditPostsFragment.class.getSimpleName();

    private int count = 0;
    private int limit = 10;
    private RedditPage mPage;
    private boolean isCommentsLoading;
    private boolean isLastPage;
    private String mPostId;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_comment_list, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String presenterId = String.valueOf(hashCode());
        this.mPresenter = (RedditCommentsContract.Presenter) PresenterManager.get(presenterId);
        if(this.mPresenter!=null){
            this.mPresenter.bindView(this);
        }else {
            this.mPresenter = new RedditCommentsPresenter(this,
                    LocalDataSource.getInstance(getActivity()),
                    RemoteDataSource.getInstance(getActivity()));
            PresenterManager.set(presenterId, this.mPresenter);
        }

        mPage = new RedditPage();
        mPage.setCount(0);
        mPage.setLimit(Constant.HOT_REDDIT_COMMENTS_FETCH_LIMIT);

        Bundle args = getArguments();
        if(args != null) {
            mPostId = String.valueOf(args.get(Extra.REDDIT_POST_ID));
            mPresenter.getNewRedditComments(mPostId, mPage);
        }

        initRecyclerView();
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {

            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getActivity().onBackPressed();

        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void setPresenter(RedditCommentsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showLoader() {

        isCommentsLoading = true;
        mProgressSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {

        isCommentsLoading = false;
        mProgressSpinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onNewRedditCommentsFetched(List<RedditComment> comments, RedditPage page) {

        Log.d(TAG, "onNewRedditCommentsFetched: "+comments.size()+" comments found");
        mRecyclerViewAdapter.setComments(comments, true);
        if(comments.size() == 0) {
            mNoCommentsText.setVisibility(View.VISIBLE);
        } else if (comments.size() >= Constant.HOT_REDDIT_COMMENTS_FETCH_LIMIT) {
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void onError(Throwable t) {
        Log.d(TAG, "onError: "+String.valueOf(t.getMessage()));
    }

    private void initRecyclerView() {

        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        Log.d(TAG, "initRecyclerView: "+mRecyclerView);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                Log.d(TAG,"visible="+visibleItemCount+", total="+totalItemCount+", first="+firstVisibleItemPosition);

                if (!isCommentsLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Constant.HOT_REDDIT_POSTS_FETCH_LIMIT) {

                        int commentOffset = mPage.getCount() + Constant.HOT_REDDIT_COMMENTS_FETCH_LIMIT;
                        mPage.setCount(commentOffset);
                        mPresenter.getNewRedditComments(mPostId, mPage);
                        Log.d(TAG, "Fetching reddit posts, page offset="+count);
                    }
                }
            }
        });

        mRecyclerViewAdapter = new RedditCommentsRecyclerViewAdapter(getActivity());
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

}
