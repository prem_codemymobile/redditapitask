package com.cmm.redditapitask.data.statics;

/**
 * Created by ankit on 10/12/17.
 */

public class Constant {

    public static final int HOT_REDDIT_POSTS_FETCH_LIMIT = 10;
    public static final int HOT_REDDIT_COMMENTS_FETCH_LIMIT = 10;
}
