package com.cmm.redditapitask.data.source.remote;

import android.content.Context;
import android.util.Log;

import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditListResponse;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.data.source.remote.callbacks.GetRedditResponseCallback;
import com.google.gson.Gson;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Akhilesh on 23-11-2016.
 */

public class RemoteDataSource implements RemoteData {

    private static final String TAG = RemoteDataSource.class.getSimpleName();
    private static RemoteDataSource mInstance = null;
    private ApiEndPointInterface endPointInterface;
    private Context mContext;

    private String youtubeVideoSearchNextPageToken;

    private RemoteDataSource(Context context){
        mContext = context;
        NetModule mNetModule = NetModule.getInstance(context);
        endPointInterface = mNetModule.createService(ApiEndPointInterface.class);
    }

    public static RemoteDataSource getInstance(Context context){
        if(mInstance == null){
            mInstance = new RemoteDataSource(context);
        }

        return mInstance;
    }

    @Override
    public void getNewRedditPosts(int count, int limit, final GetRedditResponseCallback<List<RedditPost>> callback) {

        Log.d(TAG, "getNewRedditPosts: Getting new reddits............, count="+count);

        Call<RedditListResponse> getRedditList = endPointInterface.getNewRedditPosts(count, limit);
        getRedditList.enqueue(new Callback<RedditListResponse>() {
            @Override
            public void onResponse(Call<RedditListResponse> call, Response<RedditListResponse> response) {
                if (response.isSuccessful()) {

                    Gson gson = new Gson();

                    Log.d(TAG, "onResponse: "+gson.toJson(response.body()));
                    RedditListResponse responseBody = response.body();
                    List<RedditPost> posts = new ArrayList<>();
                    List<RedditListResponse.Data.Child> children = (responseBody.getData()).getChildren();

                    RedditPage page = new RedditPage();
                    page.setBefore(String.valueOf((responseBody.getData()).getBefore()));
                    page.setAfter(String.valueOf((responseBody.getData()).getAfter()));

                    Iterator<RedditListResponse.Data.Child> iterator = children.iterator();
                    while (iterator.hasNext()) {
                        RedditListResponse.Data.Child child = iterator.next();
                        RedditPost redditPost = gson.fromJson(gson.toJson(child.getData()), RedditPost.class);
                        posts.add(redditPost);
                    }

                    callback.onSuccess(posts, page);
                }else{
                    callback.onError(null);
                }

            }

            @Override
            public void onFailure(Call<RedditListResponse> call, Throwable t) {
                callback.onError(t);
            }

        });

    }

    @Override
    public void getNewRedditComments(String postId, final RedditPage page, final GetRedditResponseCallback<List<RedditComment>> callback) {


        Log.d(TAG, "getNewRedditComments: getting new reddit comments ........");
        Call<List<RedditListResponse>> getRedditList = endPointInterface.getNewRedditComments(postId, page.getCount(), page.getLimit());
        getRedditList.enqueue(new Callback<List<RedditListResponse>>() {
            @Override
            public void onResponse(Call<List<RedditListResponse>> call, Response<List<RedditListResponse>> response) {

                Gson gson = new Gson();
                RedditListResponse redditCommentsResponse = response.body().get(1);
                List<RedditComment> posts = new ArrayList<>();
                List<RedditListResponse.Data.Child> children = (redditCommentsResponse.getData()).getChildren();

                page.setBefore(String.valueOf((redditCommentsResponse.getData()).getBefore()));
                page.setAfter(String.valueOf((redditCommentsResponse.getData()).getAfter()));

                Iterator<RedditListResponse.Data.Child> iterator = children.iterator();
                while (iterator.hasNext()) {
                    RedditListResponse.Data.Child child = iterator.next();
                    RedditComment redditComment = gson.fromJson(gson.toJson(child.getData()), RedditComment.class);
                    posts.add(redditComment);
                }
                callback.onSuccess(posts, page);
            }

            @Override
            public void onFailure(Call<List<RedditListResponse>> call, Throwable t) {
                callback.onError(t);
            }

        });
    }
}
