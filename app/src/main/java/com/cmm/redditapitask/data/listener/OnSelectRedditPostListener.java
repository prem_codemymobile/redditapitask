package com.cmm.redditapitask.data.listener;

import com.cmm.redditapitask.data.model.RedditPost;

/**
 * Created by ankit on 10/12/17.
 */

public interface OnSelectRedditPostListener {

    void onRedditPostSelected(RedditPost post);
}
