package com.cmm.redditapitask.data.source.remote;


import android.content.Context;
import android.util.Log;

import com.cmm.redditapitask.BuildConfig;
import com.cmm.redditapitask.data.source.local.LocalDataSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ankit on 08/12/17.
 */
public class NetModule {

    private static final String TAG = NetModule.class.getSimpleName();
    private static NetModule mInstance = null;
    private Retrofit retrofit;
    private OkHttpClient mOkHttpClient;
    private LocalDataSource localDataSource;

    public static NetModule getInstance(Context context){
        if(mInstance == null){
            return new NetModule(context);
        }

        return mInstance;
    }

    private NetModule(Context context){

        localDataSource = LocalDataSource.getInstance(context);
        // Add gson to retrofit
        Gson gson = new GsonBuilder().create();

        // Add Ok http client to retrofit
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
                Log.d(TAG, "intercept: "+String.valueOf(original.url()));

                return chain.proceed(requestBuilder.build());
            }
        });

        mOkHttpClient = httpClient.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(mOkHttpClient)
                .build();
    }


    public  <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
