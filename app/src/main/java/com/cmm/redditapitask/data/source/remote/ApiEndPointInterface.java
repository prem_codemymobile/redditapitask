package com.cmm.redditapitask.data.source.remote;

import com.cmm.redditapitask.data.model.RedditListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ankit on 08/12/17.
 */
public interface ApiEndPointInterface {

    //get request for fetching new reddit posts
    @GET("r/all/new.json")
    Call<RedditListResponse> getNewRedditPosts(@Query("count") int count, @Query("limit") int limit);

    //get request for fetching new reddit comments
    @GET("r/all/comments/{postId}.json")
    Call<List<RedditListResponse>> getNewRedditComments(@Path("postId") String postId, @Query("count") int count, @Query("limit") int limit);
}