package com.cmm.redditapitask.data.source.remote.callbacks;

import com.cmm.redditapitask.data.model.RedditPage;

/**
 * Created by ankit on 09/12/17.
 */

public interface GetRedditResponseCallback<T> {

    void onError(Throwable throwable);

    void onSuccess(T body, RedditPage page);
}
