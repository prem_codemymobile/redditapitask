package com.cmm.redditapitask.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by ankit on 11/12/17.
 */

public class RedditListResponse {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        @SerializedName("children")
        @Expose
        private List<Child> children = null;
        @SerializedName("after")
        @Expose
        private Object after;
        @SerializedName("before")
        @Expose
        private Object before;

        public List<Child> getChildren() {
            return children;
        }

        public void setChildren(List<Child> children) {
            this.children = children;
        }

        public Object getAfter() {
            return after;
        }

        public void setAfter(Object after) {
            this.after = after;
        }

        public Object getBefore() {
            return before;
        }

        public void setBefore(Object before) {
            this.before = before;
        }

        public static class Child {

            @SerializedName("kind")
            @Expose
            private String kind;
            @SerializedName("data")
            @Expose
            private ChildData data;

            public String getKind() {
                return kind;
            }

            public void setKind(String kind) {
                this.kind = kind;
            }

            public ChildData getData() {
                return data;
            }

            public void setData(ChildData data) {
                this.data = data;
            }

            public static class ChildData {

                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("title")
                @Expose
                private String title;
                @SerializedName("thumbnail")
                @Expose
                private String thumbnail;
                @SerializedName("thumbnail_height")
                @Expose
                private int thumbnailHeight;
                @SerializedName("thumbnail_width")
                @Expose
                private int thumbnailWidth;
                @SerializedName("num_comments")
                @Expose
                private int numComments;
                @SerializedName("permalink")
                @Expose
                private String permalink;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("parent_id")
                @Expose
                private String parentId;
                @SerializedName("body")
                @Expose
                private String body;
                @SerializedName("body_html")
                @Expose
                private String bodyHtml;
                @SerializedName("likes")
                @Expose
                private Object likes;
                @SerializedName("replies")
                @Expose
                private String replies;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getThumbnail() {
                    return thumbnail;
                }

                public void setThumbnail(String thumbnail) {
                    this.thumbnail = thumbnail;
                }

                public int getThumbnailHeight() {
                    return thumbnailHeight;
                }

                public void setThumbnailHeight(int thumbnailHeight) {
                    this.thumbnailHeight = thumbnailHeight;
                }

                public int getThumbnailWidth() {
                    return thumbnailWidth;
                }

                public void setThumbnailWidth(int thumbnailWidth) {
                    this.thumbnailWidth = thumbnailWidth;
                }

                public int getNumComments() {
                    return numComments;
                }

                public String getPermalink() {
                    return permalink;
                }

                public void setPermalink(String permalink) {
                    this.permalink = permalink;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setNumComments(int numComments) {
                    this.numComments = numComments;
                }

                public String getParentId() {
                    return parentId;
                }

                public void setParentId(String parentId) {
                    this.parentId = parentId;
                }

                public String getBody() {
                    return body;
                }

                public void setBody(String body) {
                    this.body = body;
                }

                public String getBodyHtml() {
                    return bodyHtml;
                }

                public void setBodyHtml(String bodyHtml) {
                    this.bodyHtml = bodyHtml;
                }

                public Object getLikes() {
                    return likes;
                }

                public void setLikes(Object likes) {
                    this.likes = likes;
                }

                public String getReplies() {
                    return replies;
                }

                public void setReplies(String replies) {
                    this.replies = replies;
                }
            }

        }

    }


}








