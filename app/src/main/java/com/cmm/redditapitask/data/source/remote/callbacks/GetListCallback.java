package com.cmm.redditapitask.data.source.remote.callbacks;

import java.util.List;

/**
 * Created by ankit on 09/12/17.
 */
public interface GetListCallback<T> {

    void onLoad(List<T> list);

    void onError(Throwable throwable);
}
