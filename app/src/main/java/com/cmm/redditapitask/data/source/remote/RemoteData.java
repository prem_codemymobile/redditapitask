package com.cmm.redditapitask.data.source.remote;


import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditPage;
import com.cmm.redditapitask.data.model.RedditPost;
import com.cmm.redditapitask.data.source.remote.callbacks.GetRedditResponseCallback;

import java.util.List;

/**
 * Created by ankit on 08/12/17.
 */
public interface RemoteData {

    void getNewRedditPosts(int count, int limit, GetRedditResponseCallback<List<RedditPost>> callback);

    void getNewRedditComments(String postId, RedditPage page, GetRedditResponseCallback<List<RedditComment>> callback);
}
