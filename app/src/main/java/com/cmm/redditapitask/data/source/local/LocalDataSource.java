package com.cmm.redditapitask.data.source.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Akhilesh on 23-11-2016.
 */

public class LocalDataSource implements LocalData {

    private static LocalDataSource INSTANCE = null;
    private final Context mContext;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences mPrivateSharedPrefs;

    private LocalDataSource(Context context) {
        mContext = context;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mPrivateSharedPrefs = context.getSharedPreferences("Preference Private", Context.MODE_PRIVATE);
    }

    public static LocalDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LocalDataSource(context);
        }
        return INSTANCE;
    }

}
