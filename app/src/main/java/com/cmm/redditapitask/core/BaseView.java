package com.cmm.redditapitask.core;

import android.content.Context;

/**
 * Created by ankit on 08/12/17.
 */
public interface BaseView<T> {

    Context getContext();
    void setPresenter(T presenter);
}
