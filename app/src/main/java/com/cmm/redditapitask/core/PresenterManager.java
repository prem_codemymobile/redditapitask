package com.cmm.redditapitask.core;

import java.util.HashMap;

/**
 * Created by ankit on 08/12/17.
 */

public class PresenterManager {

    private static HashMap<String, BasePresenter> presenters = new HashMap<>();

    public static BasePresenter get(String presenterId) {

        return presenters.get(presenterId);
    }

    public static void set(String presenterId, BasePresenter presenter) {
        presenters.put(presenterId, presenter);
    }

    public static void remove(String presenterId) {
        if(presenters.containsKey(presenterId)) presenters.remove(presenterId);
    }
}
