package com.cmm.redditapitask.core;

/**
 * Created by ankit on 08/12/17.
 */

public interface BasePresenter<T> {

    void init();
    void bindView(T view);
    void unbindView();
}
