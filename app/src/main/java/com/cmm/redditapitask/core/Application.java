package com.cmm.redditapitask.core;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.cmm.redditapitask.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ankit on 08/12/17.
 */

public class Application extends android.app.Application {

    private static Application mInstance;

    public static Application getInstance(){
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        //super.attachBaseContext(base);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getResources().getString(R.string.font_open_sans_regular))
                .setFontAttrId(R.attr.fontPath)
                .build());

        Log.d("Application", "onCreate: Calligraphy font initialized");
    }
}
