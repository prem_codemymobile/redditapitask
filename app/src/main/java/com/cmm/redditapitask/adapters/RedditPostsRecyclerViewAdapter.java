package com.cmm.redditapitask.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cmm.redditapitask.R;
import com.cmm.redditapitask.data.listener.OnSelectRedditPostListener;
import com.cmm.redditapitask.data.model.RedditPost;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ankit on 10/12/17.
 */

public class RedditPostsRecyclerViewAdapter extends RecyclerView.Adapter<RedditPostsRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private List<RedditPost> mRedditList = new ArrayList<>();
    private List<RedditPostsRecyclerViewAdapter.ViewHolder> viewHolders = new ArrayList<>();
    private static final String TAG = RedditPostsRecyclerViewAdapter.class.getSimpleName();

    private OnSelectRedditPostListener mListener;
    private int postOffset = 0;

    public RedditPostsRecyclerViewAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_reddit_post, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final RedditPost post = mRedditList.get(position);

        holder.mTitle.setText(post.getTitle());
        String thumb = post.getThumbnail();
        if(thumb.equals("default")) {
            Glide.with(mContext).load(R.drawable.reddit_logo).centerCrop().into(holder.mThumb);
        } else {
            Glide.with(mContext).load(thumb).centerCrop().into(holder.mThumb);
        }

        if(mListener !=null) {
            holder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onRedditPostSelected(post);
                }
            });
        }

        viewHolders.add(holder);
    }

    @Override
    public int getItemCount() {

        return (mRedditList != null)? mRedditList.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.post_title)
        TextView mTitle;

        @BindView(R.id.post_thumb)
        CircleImageView mThumb;

        private View mView = null;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            ButterKnife.bind(this,itemView);
        }

        public View getView() { return mView; }
    }

    public void setReddits(List<RedditPost> posts, int offset, boolean isAppend) {

        if(isAppend) this.mRedditList.addAll(posts);
        else this.mRedditList = posts;
        this.postOffset = offset;
        this.notifyDataSetChanged();
    }

    public void setmListener(OnSelectRedditPostListener mListener) {
        this.mListener = mListener;
    }

}
