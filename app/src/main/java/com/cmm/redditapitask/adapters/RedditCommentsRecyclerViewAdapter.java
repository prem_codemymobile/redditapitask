package com.cmm.redditapitask.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cmm.redditapitask.R;
import com.cmm.redditapitask.data.listener.OnSelectRedditPostListener;
import com.cmm.redditapitask.data.model.RedditComment;
import com.cmm.redditapitask.data.model.RedditPost;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ankit on 11/12/17.
 */

public class RedditCommentsRecyclerViewAdapter extends RecyclerView.Adapter<RedditCommentsRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private List<RedditComment> mRedditList = new ArrayList<>();
    private static final String TAG = RedditPostsRecyclerViewAdapter.class.getSimpleName();

    private OnSelectRedditPostListener mListener;
    private int postOffset = 0;

    public RedditCommentsRecyclerViewAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_reddit_comment, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final RedditComment comment = mRedditList.get(position);

        holder.mCommentBody.setText(comment.getBody());

    }

    @Override
    public int getItemCount() {

        return (mRedditList != null)? mRedditList.size():0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.comment_body)
        TextView mCommentBody;

        private View mView = null;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            ButterKnife.bind(this,itemView);
        }

        public View getView() { return mView; }
    }

    public void setComments(List<RedditComment> comments, boolean isAppend) {

        if(isAppend) this.mRedditList.addAll(comments);
        else this.mRedditList = comments;
        this.notifyDataSetChanged();
    }

}

